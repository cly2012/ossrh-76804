package com.cly.core;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"com.cly.core.aspect"})
public class TransformFieldAutoConfiguration {
}
