package com.cly.core.aspect;

import com.cly.core.ReflectUtil;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
@Aspect
public class TransformFieldsValueAspect {

    @Autowired
    private BeanUtil beanUtil;

    /**
     * 环绕通知：目标方法执行前后分别执行一些代码，发生异常的时候执行另外一些代码
     *
     * @param pjp
     * @return
     * @throws Throwable
     */
    @Around("@annotation(com.cly.core.annotations.TransformField)")
    public Object doTransformFieldsValue(ProceedingJoinPoint pjp) throws Throwable {

        //执行目标方法
        Object ret = pjp.proceed();

        if(ret==null)
            return null;

        if (ret instanceof Collection) {
            beanUtil.transformForCollection((Collection) ret);
        }else {
                Object tmp = ReflectUtil.getFieldValue(ret,"data");

                if(tmp!=null) {
                    if (tmp instanceof Collection) {
                        beanUtil.transformForCollection((Collection) tmp);
                    } else {
                        Object temps = ReflectUtil.getFieldValue(tmp,"records");

                        if (temps!=null) {
                            beanUtil.transformForCollection((Collection) temps);
                        } else {
                            beanUtil.transformForObject(tmp);
                        }
                    }
                }
                else {
                    beanUtil.transformForObject(ret);
                }
        }

        return ret;
    }
}
