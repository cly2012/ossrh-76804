package com.cly.core.aspect;

import com.cly.core.ReflectUtil;
import com.cly.core.annotations.TransformFieldProperty;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component
public class BeanUtil implements ApplicationContextAware {

    public static final String METHOD_GET = "get";
    public static final String METHOOD_SET = "set";
    public static final String COMMA = ",";
    private ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext)
            throws BeansException {
        if (this.applicationContext == null) {
            this.applicationContext = applicationContext;
            System.out.println("--------SpringUtil.applicationContext  ok ---------");
        }
    }

    private Object getValueFromMethod(Object obj, Class<?> clazz, com.cly.core.annotations.TransformFieldProperty tfp, Field field) throws InvocationTargetException, IllegalAccessException, NoSuchMethodException, NoSuchFieldException {

        Object bean = this.applicationContext.getBean(tfp.beanClass());

        //肯定有参数值
        String[] fileds = tfp.argsFields().split(COMMA);
        // 要调用的方法
        int targetFieldLen = fileds.length;
        Class<?>[] fieldClazzs = new Class<?>[targetFieldLen];
        for (int i = 0; i < targetFieldLen; i++) {
            fieldClazzs[i] = ReflectUtil.getAccessibleField(clazz, fileds[i]).getType();
        }
        Method method = tfp.beanClass().getMethod(tfp.handler(), fieldClazzs);

        //获取给入参的值的获取方法
        Object[] fieldValues = new Object[targetFieldLen];
        for (int i = 0; i < targetFieldLen; i++) {
            Method targetValueGetMethod = clazz.getMethod(METHOD_GET + StringUtils.capitalize(fileds[i]));

            Object paramValue = targetValueGetMethod.invoke(obj);
            fieldValues[i] = paramValue;
        }
        return method.invoke(bean, fieldValues);
    }

    private void setValueFromEnum(Object obj, Class<?> clazz, TransformFieldProperty tfp, Field field) throws InvocationTargetException, IllegalAccessException, NoSuchMethodException {

        // 获取给入参的值的获取方法
        Method paramValueGetMethod = clazz.getMethod(
                METHOD_GET + StringUtils.capitalize(tfp.argsFields()));

        Method setValueMethod = clazz.getMethod(
                METHOOD_SET + StringUtils.capitalize(field.getName()), field.getType());

        // 获取到入参的参数值
        Object paramValue = paramValueGetMethod.invoke(obj);
        if (paramValue == null) {
            return;
        }
        // 要调用的enum
        Method enumMethod = null;
        if (Integer.class.isInstance(paramValue) || Byte.class.isInstance(paramValue)) {
            enumMethod = tfp.beanClass().getMethod(tfp.handler(), int.class);
        } else if (String.class.isInstance(paramValue)) {
            enumMethod = tfp.beanClass().getMethod(tfp.handler(), String.class);
        }

        Object value = enumMethod.invoke(null, paramValue);
        setValueMethod.invoke(obj, value);
    }

    private void setValueFromObject(Object obj, Class<?> clazz, TransformFieldProperty tfp, Field field) throws InvocationTargetException, IllegalAccessException, NoSuchMethodException, NoSuchFieldException, InstantiationException {

        //返回值
        Object value = getValueFromMethod(obj, clazz, tfp, field);
        Method setValueMethod = clazz.getMethod(METHOOD_SET + StringUtils.capitalize(field.getName()), field.getType());


        boolean isCollect = field.getType().equals(List.class);//被设置的字段是否是集合
        Class<?> clazz1 = null;
        if (isCollect) {
            //反射list集合里面的对象的类型
            clazz1 = (Class<?>) ((java.lang.reflect.ParameterizedType) field.getGenericType()).getActualTypeArguments()[0];
        }
        //被设置的字段是否是内部属性赋值
        boolean isInner = StringUtils.isNotBlank(tfp.targetAttr());

        //字段是否是集合
        if (isCollect) {
            Collection collection2 = (Collection) value;
            List<Object> collection1 = new ArrayList<>();
            for (Object obj2 : collection2) {
                if (isInner) {//返回是对象，被设置的字段是返回对象里面的属性 比如返回user实体，赋值user.name
                    Object obj1 = ReflectUtil.getFieldValue(value, tfp.targetAttr());
                    collection1.add(obj1);
                } else {//返回类型一样，直接覆盖
                    Object obj1 = clazz1.newInstance();
                    BeanUtils.copyProperties(obj2, obj1);
                    collection1.add(obj1);
                }
            }
            setValueMethod.invoke(obj, collection1);
        } else {//被设置的字段不是集合
            if (isInner) {//返回是对象，被设置的字段是返回对象里面的属性 比如返回user实体，赋值user.name
                setValueMethod.invoke(obj, (Object) ReflectUtil.getFieldValue(value, tfp.targetAttr()));
            } else {//返回类型一样，直接覆盖
                setValueMethod.invoke(obj, value);
            }
        }
    }


    public void transformForObject(Object obj) throws NoSuchMethodException, NoSuchFieldException, IllegalAccessException, InvocationTargetException, InstantiationException {

        if (obj == null)
            return;

        // 1获得clazz对象
        Class<?> clazz = obj.getClass();
        // 2、获取里面的字段
        Field[] fields = clazz.getDeclaredFields();

        // 3、遍历处理带有指定注解的字段
        for (Field field : fields) {
            // 3.1 获取字段的TransformFieldProperty注解
            TransformFieldProperty tfp = field.getAnnotation(TransformFieldProperty.class);
            if (tfp == null) {
                continue;
            }

            //调用类是否是枚举类
            boolean isEnum = tfp.beanClass().isEnum();
            if (isEnum) {
                setValueFromEnum(obj, clazz, tfp, field);
            } else {
                setValueFromObject(obj, clazz, tfp, field);
            }
        }

    }

    //------------------------------------------------------- 以下是返回对象是集合---------------------------------------------------
    private void setCollectValueFromEnum(Collection col, Class<?> clazz, TransformFieldProperty tfp, Field field) throws InvocationTargetException, IllegalAccessException, NoSuchMethodException {

        // 获取给入参的值的获取方法
        Method paramValueGetMethod = clazz.getMethod(
                METHOD_GET + StringUtils.capitalize(tfp.argsFields()));

        Method setValueMethod = clazz.getMethod(
                METHOOD_SET + StringUtils.capitalize(field.getName()), field.getType());

        // 要调用的enum
        Method enumMethod = null;

        for (Object obj : col) {
            // 获取到入参的参数值
            Object paramValue = paramValueGetMethod.invoke(obj);
            if (paramValue == null) {
                continue;
            }
            if (enumMethod == null) {
                if (Integer.class.isInstance(paramValue) || Byte.class.isInstance(paramValue)) {
                    enumMethod = tfp.beanClass().getMethod(tfp.handler(), int.class);
                } else if (String.class.isInstance(paramValue)) {
                    enumMethod = tfp.beanClass().getMethod(tfp.handler(), String.class);
                }
            }

            Object value = enumMethod.invoke(null, paramValue);
            setValueMethod.invoke(obj, value);
        }
    }

    private void setCollectValueFromObject(Collection col, Class<?> clazz, TransformFieldProperty tfp, Field field) throws InvocationTargetException, IllegalAccessException, NoSuchMethodException, NoSuchFieldException, InstantiationException {

        Object bean = this.applicationContext.getBean(tfp.beanClass());

        //肯定有参数值
        String[] fileds = tfp.argsFields().split(COMMA);
        // 要调用的方法
        int targetFieldLen = fileds.length;
        Class<?>[] fieldClazzs = new Class<?>[targetFieldLen];
        for (int i = 0; i < targetFieldLen; i++) {
            fieldClazzs[i] = ReflectUtil.getAccessibleField(clazz, fileds[i]).getType();
        }
        Method method = tfp.beanClass().getMethod(tfp.handler(), fieldClazzs);

        boolean isCollect = field.getType().equals(List.class);//被设置的字段是否是集合
        Class<?> clazz1 = null;
        if (isCollect) {
            //反射list集合里面的对象的类型
            clazz1 = (Class<?>) ((java.lang.reflect.ParameterizedType) field.getGenericType()).getActualTypeArguments()[0];
        }
        //被设置的字段是否是内部属性赋值
        boolean isInner = StringUtils.isNotBlank(tfp.targetAttr());

        // 4 遍历对象
        for (Object obj : col) {
            //获取给入参的值的获取方法
            Object[] fieldValues = new Object[targetFieldLen];
            for (int i = 0; i < targetFieldLen; i++) {
                Method targetValueGetMethod = clazz.getMethod(METHOD_GET + StringUtils.capitalize(fileds[i]));

                Object paramValue = targetValueGetMethod.invoke(obj);
                fieldValues[i] = paramValue;
            }

            //获取值
            Object value = method.invoke(bean, fieldValues);
            Method setValueMethod = clazz.getMethod(METHOOD_SET + StringUtils.capitalize(field.getName()), field.getType());

            //被设置的字段是否是集合
            if (isCollect) {
                Collection collection2 = (Collection) value;

                List<Object> collection1 = new ArrayList<>();
                for (Object obj2 : collection2) {
                    if (isInner) {//返回是对象，被设置的字段是返回对象里面的属性 比如返回user实体，赋值user.name
                        Object obj1 = ReflectUtil.getFieldValue(value, tfp.targetAttr());
                        collection1.add(obj1);
                    } else {//返回类型一样，直接覆盖
                        Object obj1 = clazz1.newInstance();
                        BeanUtils.copyProperties(obj2, obj1);
                        collection1.add(obj1);
                    }
                }
                setValueMethod.invoke(obj, collection1);
            } else {//被设置的字段不是集合
                if (isInner) {//返回是对象，被设置的字段是返回对象里面的属性 比如返回user实体，赋值user.name
                    setValueMethod.invoke(obj, (Object) ReflectUtil.getFieldValue(value, tfp.targetAttr()));
                } else {//返回类型一样，直接覆盖
                    setValueMethod.invoke(obj, value);
                }
            }

        }
    }

    /**
     * 列表转换内部字段
     *
     */
    public void transformForCollection(Collection col) throws InvocationTargetException, NoSuchMethodException, IllegalAccessException, NoSuchFieldException, InstantiationException {
        if (CollectionUtils.isEmpty(col)) {
            return;
        }

        // 1获得clazz对象
        Class<?> clazz = col.iterator().next().getClass();

        // 2、获取里面的字段
        Field[] fields = clazz.getDeclaredFields();

        // 3、遍历处理带有指定注解的字段
        for (Field field : fields) {
            // 3.1 获取字段的TransformFieldProperty注解
            TransformFieldProperty tfp = field.getAnnotation(TransformFieldProperty.class);
            if (tfp == null) {
                continue;
            }

            //调用类是否是枚举类
            boolean isEnum = tfp.beanClass().isEnum();
            if (isEnum) {
                setCollectValueFromEnum(col, clazz, tfp, field);
            } else {
                setCollectValueFromObject(col, clazz, tfp, field);
            }
        }

    }

    /**
     * 判断一个对象是否是基本类型或基本类型的封装类型
     */
    private boolean isPrimitive(Object obj) {
        try {
            return ((Class<?>) obj.getClass().getField("TYPE").get(null)).isPrimitive();
        } catch (Exception e) {
            return false;
        }
    }
}
