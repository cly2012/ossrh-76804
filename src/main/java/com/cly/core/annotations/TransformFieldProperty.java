package com.cly.core.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface TransformFieldProperty {

    /**
     * 指定用于转换的bean的类
     * 可以是枚举类、静态类、普通类
     * @return Class
     */
    Class<?> beanClass() ;

    /**
     * bean的哪个方法
     * 若beanClass为枚举类，则是获取名称的方法，若beanClass为其他，则需指定
     * @return String
     */
    String handler() default "getDescByValue";

    /**
     * 原对象上哪个字段，取名称，多个按逗号隔开 ，作为方法传入的参数
     * @return String
     */
    String argsFields() default "";

    /**
     * 映射内部对象的哪个字段
     * eg:handler方法返回的是实体类User，只然后赋值user的属性userName
     * @return String
     */
    String targetAttr() default "";
}